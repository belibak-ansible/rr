#!/bin/bash
set -x
if [[ -e venv ]]; then rm -rf venv; fi
virtualenv -p python3 venv
venv/bin/pip install -r requirements.txt
