#!/usr/bin/env python3
import selenium
import datetime
import argparse
import sys
import time
import random

from selenium import webdriver

from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

import config
from kill import kill

'''
options = Options()
options.headless = True
driver = webdriver.Firefox(options=options)
driver.get("http://google.com/")

config.py:
login=""
passwd=""

wwidth = 800
wheight = 600

kill_time = (20, 0)
'''

url = 'https://www.rockradio.com/symphonicmetal'

def randomize_genre():
    from genres import genres
    global url
    url = 'https://www.rockradio.com/%s' %(random.choice(genres))
    print(url)

dr = webdriver.Firefox()
dr.set_window_position(0, 0)
dr.set_window_size(config.wwidth, config.wheight)


while 1:
    try:
        dr.get('https://www.rockradio.com')
        break
    except BrokenPipeError:
        pass

def login():
    dr.find_element_by_xpath('/html/body/div[5]/div/i[2]').click()

    dr.find_element_by_xpath('/html/body/div[2]/header/div/nav[2]/ul/li[2]/a').click()

    set_login = dr.find_element_by_xpath('/html/body/div[3]/div/div/div[3]/ul/li[1]/div/div/div/form/ul/li[1]/input')
    set_login.clear()
    set_login.send_keys(config.login)

    set_pass = dr.find_element_by_xpath("/html/body/div[3]/div/div/div[3]/ul/li[1]/div/div/div/form/ul/li[2]/input")
    set_pass.clear()
    set_pass.send_keys(config.passwd)
    sleep(0.5)

    dr.find_element_by_xpath("/html/body/div[3]/div/div/div[3]/ul/li[1]/div/div/div/form/ul/li[3]/input").click()
    sleep(0.5)

def get_play():
    dr.get(url)

def click_on_play():
    play = dr.find_element_by_xpath("/html/body/div[2]/div[5]/div/section/section[2]/div/div/div[2]/div/a")
    icon = play.get_attribute('class')
    play_button = dr.find_element_by_xpath('/html/body/div[2]/div[5]/div/section/section[2]/div/div/div[2]/div')
    if icon == 'ico icon-play':
        play_button.click()
        print('play clicked')


def reload_ads(dr):
    try:
        dr.find_element_by_xpath("/html/body/div[2]/div[5]/div/section/section[3]/div[2]/div[2]").click()      #dr.find_element_by_class_name("close-spinner icon-spinner2").click()
        dr.refresh()
        print('%s adv closed' %(datetime.datetime.now().strftime("[%d-%m-%Y %H:%M:%S]")))
        click_on_play()
    except selenium.common.exceptions.ElementNotInteractableException as e:
        pass #print(e)
    except selenium.common.exceptions.NoSuchWindowException:
        print('Message: Browsing context has been discarded')
    except selenium.common.exceptions.StaleElementReferenceException as e:
        print('selenium.common.exceptions.StaleElementReferenceException - 93', e)


def adblock(dr):
    try:
        dr.find_element_by_id("adblock-wall-cta")
        dr.refresh()
    except selenium.common.exceptions.ElementNotInteractableException as e:
        pass #print(e)
    except selenium.common.exceptions.WebDriverException as e:
        if 'marionette' in str(e):
            print("Browsesr closed", e)
            sys.exit(0)

def test(random=False):
    if random:
        randomize_genre()
    login()
    get_play()
    screen_name = "/tmp/passed-%s-%s.png" %(int(time.time()), 'rockradio')
    try:
        dr.find_element_by_xpath("/html/body/div[2]/div[5]/div/section/section[2]/div/div/div[2]/div/a")
        print("Test passed\nScreenshot saved as: %s" %screen_name)
        dr.save_screenshot(screen_name)
        dr.quit()
        sys.exit(0)
    except selenium.common.exceptions.NoSuchElementException:
        print("Something went wrong\nScreenshot saved as: %s" %screen_name)
        dr.save_screenshot(screen_name)
        dr.quit()
        sys.exit(1)



def main():
    login()
    get_play()
    while 1:
        try:
            reload_ads(dr)
            adblock(dr)
            dr.find_element_by_class_name("close").click()
        except BrokenPipeError:
            continue
        except selenium.common.exceptions.NoSuchElementException:
            sleep(0.1)
            if kill(config.kill_time[0], config.kill_time[1]):
                dr.quit()
                print("Killed in %s:%s" %(config.kill_time[0], config.kill_time[1]))
                sys.exit(0)
                break
            # print("Searching again")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', action="store_true")
    parser.add_argument('--random', action="store_true")
    args = parser.parse_args()
    if args.t:
        print(args)
        test()
    else:
        if args.random:
            randomize_genre()
        main()



