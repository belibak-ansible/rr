#!/usr/bin/env python3

with open('/tmp/channels.txt', 'r') as file:
    raws = file.readline()
    raws = raws.split(" ")
    genres = [ i.split('=')[1].replace("\"", '') for i in raws if 'data-channel-key' in i ]
    print(genres)
    genres_file = open('genres.txt', 'w')
    for i in genres:
        genres_file.write("%s\n" %(i))
    genres_file.close()
